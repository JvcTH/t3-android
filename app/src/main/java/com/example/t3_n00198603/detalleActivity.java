package com.example.t3_n00198603;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class detalleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        poke model = (poke) getIntent().getSerializableExtra("Poke");
        assert model != null;

        ImageView imagen = findViewById(R.id.imagen);
        TextView nombre = findViewById(R.id.nombre);
        TextView tipo = findViewById(R.id.tipo);

        Picasso.get()
                .load(model.getUrl_imagen())
                .resize(50, 50)
                .centerCrop()
                .into(imagen);

        nombre.setText(model.getNombre());
        tipo.setText(model.getTipo());

    }
}