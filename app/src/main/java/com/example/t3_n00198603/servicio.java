package com.example.t3_n00198603;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface servicio {

    @GET("N00198603")
    Call<List<poke>> getAll();

    @POST("N00198603/crear")
    Call<Void> POST(@Body poke pok);


}
